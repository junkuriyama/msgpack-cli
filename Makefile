# Copyright (c) 2020  S2 Factory, Inc.  All rights reserved.

DATE:=	`date +%Y%m%d`

image:
	docker build -t kuriyama/docker-msgpack-cli:latest -f Dockerfile .

pull:
	@./check.sh golang 1
	@curl -s -H "Accept: application/vnd.github.v3+json" https://api.github.com/repos/jakm/msgpack-cli/branches/master | jq -r .commit.sha > .id-msgpack-cli

check:
	@if git status -s | grep -q M; then\
		git add .id-* && git commit -m "Update IDs."; git push;\
		git tag ${DATE}; git push origin ${DATE};\
		git push bucket ${DATE};\
	fi
hub:
	@sleep 60
	@if docker pull ${SRC}:${DATE} 2>/dev/null; then\
		docker tag ${SRC}:${DATE} kuriyama/msgpack-cli:${DATE}; \
		docker push kuriyama/msgpack-cli:${DATE};\
	fi

cron: pull check hub
