FROM golang:1
# Use `latest` for supported Go version.

RUN go install github.com/jakm/msgpack-cli@latest

ENTRYPOINT [ "/go/bin/msgpack-cli" ]
